package bootcamp.antivir.integration.event.producer

import bootcamp.antivir.integration.event.FileCheckedEvent
import bootcamp.antivir.integration.event.initializer.FILE_CHECKED_EXCHANGE_NAME
import bootcamp.antivir.integration.event.initializer.FILE_CHECKED_ROUTING_KEY
import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient(FILE_CHECKED_EXCHANGE_NAME)
interface FileStatusChangedProducer {

    @Binding(FILE_CHECKED_ROUTING_KEY)
    suspend fun send(data: FileCheckedEvent)
}
