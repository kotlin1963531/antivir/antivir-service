package bootcamp.antivir.integration.event

import io.micronaut.core.annotation.Introspected
import io.micronaut.serde.annotation.Serdeable
import jakarta.validation.constraints.NotNull
import java.util.UUID

@Serdeable
@Introspected
data class FileUploadedEvent (
    @field:NotNull
    val id: UUID,
)
