package bootcamp.antivir.integration.event

import bootcamp.antivir.file.enumerable.FileStatus
import io.micronaut.core.annotation.Introspected
import io.micronaut.serde.annotation.Serdeable
import jakarta.validation.constraints.NotNull
import java.util.UUID

@Serdeable
@Introspected
data class FileCheckedEvent (
    @field:NotNull
    val id: UUID,
    @field:NotNull
    val status: FileStatus
)
