package bootcamp.antivir.integration.event.initializer

import com.rabbitmq.client.BuiltinExchangeType
import com.rabbitmq.client.Channel
import io.micronaut.rabbitmq.connect.ChannelInitializer
import jakarta.inject.Singleton

const val DLQ_EXCHANGE_NAME = "DLQ_EXCHANGE"

const val DLQ_FILE_CHECKED_QUEUE_NAME = "FILE_CHECKED_DLQ"
const val DLQ_FILE_CHECKED_ROUTING_KEY = "dlq.file_checked"

const val DLQ_FILE_UPLOADED_QUEUE_NAME = "FILE_UPLOADED_DLQ"
const val DLQ_FILE_UPLOADED_ROUTING_KEY = "dlq.file_uploaded"


@Singleton
class DlqChannelInitializer : ChannelInitializer() {

    override fun initialize(channel: Channel, name: String?) {
        channel.exchangeDeclare(DLQ_EXCHANGE_NAME, BuiltinExchangeType.DIRECT)
        channel.queueDeclare(DLQ_FILE_CHECKED_QUEUE_NAME, true,  false, false, null)
        channel.queueBind(DLQ_FILE_CHECKED_QUEUE_NAME, DLQ_EXCHANGE_NAME, DLQ_FILE_CHECKED_ROUTING_KEY)

        channel.queueDeclare(DLQ_FILE_UPLOADED_QUEUE_NAME, true,  false, false, null)
        channel.queueBind(DLQ_FILE_UPLOADED_QUEUE_NAME, DLQ_EXCHANGE_NAME, DLQ_FILE_UPLOADED_ROUTING_KEY)
    }

}
