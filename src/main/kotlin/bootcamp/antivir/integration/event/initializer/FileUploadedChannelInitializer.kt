package bootcamp.antivir.integration.event.initializer

import com.rabbitmq.client.BuiltinExchangeType
import com.rabbitmq.client.Channel
import io.micronaut.rabbitmq.connect.ChannelInitializer
import jakarta.inject.Singleton

const val FILE_UPLOADED_EXCHANGE_NAME = "FILE_UPLOADED_EXCHANGE"
const val FILE_UPLOADED_QUEUE_NAME = "FILE_UPLOADED_QUEUE"
const val FILE_UPLOADED_ROUTING_KEY = "file_uploaded"

@Singleton
class FileUploadedChannelInitializer : ChannelInitializer() {

    override fun initialize(channel: Channel, name: String?) {
        val dlqArgs = mapOf(
            "x-dead-letter-exchange" to DLQ_EXCHANGE_NAME,
            "x-dead-letter-routing-key" to DLQ_FILE_UPLOADED_ROUTING_KEY,
            "x-delivery-count" to 5
        )

        channel.exchangeDeclare(FILE_UPLOADED_EXCHANGE_NAME, BuiltinExchangeType.DIRECT)
        channel.queueDeclare(FILE_UPLOADED_QUEUE_NAME, true,  false, false, dlqArgs)
        channel.queueBind(FILE_UPLOADED_QUEUE_NAME, FILE_UPLOADED_EXCHANGE_NAME, FILE_UPLOADED_ROUTING_KEY)
    }

}
