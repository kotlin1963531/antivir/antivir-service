package bootcamp.antivir.integration.event.listener

import bootcamp.antivir.integration.event.FileUploadedEvent
import bootcamp.antivir.integration.event.initializer.FILE_UPLOADED_QUEUE_NAME
import bootcamp.antivir.file.service.AntivirusService
import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitListener
import io.micronaut.rabbitmq.bind.RabbitAcknowledgement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

@RabbitListener
class FileUploadedEvent(
    private val antivirusService: AntivirusService
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO)

    @Queue(FILE_UPLOADED_QUEUE_NAME)
    fun handleFileUploadedEvent(event: FileUploadedEvent, acknowledgement: RabbitAcknowledgement) {
        scope.launch {
            logger.info("FileUploadedEvent.handleFileUploadedEvent - File ${event.id} comes for scanning")
            try {
                antivirusService.scan(event.id)
                acknowledgement.ack()
            } catch (e: Exception) {
                logger.error("FileUploadedEvent.handleRadarDataEvent - Error during scanning", e)
                acknowledgement.nack(false, true)
            }
        }
    }
}

