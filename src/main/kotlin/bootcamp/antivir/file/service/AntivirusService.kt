package bootcamp.antivir.file.service

import bootcamp.antivir.file.adapter.client.AntivirusClient
import bootcamp.antivir.file.adapter.storage.FileStorage
import bootcamp.antivir.integration.event.FileCheckedEvent
import bootcamp.antivir.integration.event.producer.FileStatusChangedProducer
import bootcamp.antivir.file.enumerable.FileStatus
import jakarta.inject.Singleton
import java.util.UUID

@Singleton
class AntivirusService(
    private val fileStorage: FileStorage,
    private val antivirusClient: AntivirusClient,
    private val fileStatusChangedProducer: FileStatusChangedProducer
) {

    suspend fun scan(fileId: UUID): FileStatus {
        val file = fileStorage.getFile(fileId)

        val status = antivirusClient.check(file)

        fileStatusChangedProducer.send(FileCheckedEvent(fileId, status))

        return status
    }
}
