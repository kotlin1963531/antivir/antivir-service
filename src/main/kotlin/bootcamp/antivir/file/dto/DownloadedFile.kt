package bootcamp.antivir.file.dto

import io.micronaut.core.annotation.Introspected
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import java.io.InputStream

@Introspected
data class DownloadedFile(
    @field:NotBlank
    val name: String,
    @field:NotNull
    val content: InputStream,
)
