package bootcamp.antivir.file.adapter.client

import bootcamp.antivir.file.dto.DownloadedFile
import bootcamp.antivir.file.enumerable.FileStatus

interface AntivirusClient {
    suspend fun check(file: DownloadedFile): FileStatus
}
