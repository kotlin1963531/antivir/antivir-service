package bootcamp.antivir.file.adapter.client

import bootcamp.antivir.file.dto.DownloadedFile
import bootcamp.antivir.file.enumerable.FileStatus
import jakarta.inject.Singleton

@Singleton
class MockAntivirusClient : AntivirusClient {
    private var callCount = 0

    override suspend fun check(file: DownloadedFile): FileStatus {
        callCount++
        return if (callCount % 2 == 0) FileStatus.INFECTED else FileStatus.NOT_INFECTED
    }
}
