package bootcamp.antivir.file.adapter.storage

import bootcamp.antivir.file.dto.DownloadedFile
import jakarta.inject.Singleton
import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import java.util.UUID

@Singleton
class MockFileStorage : FileStorage {
    override suspend fun getFile(fileId: UUID): DownloadedFile {
        val dummyContent = "This is a dummy string for the input stream."

        val stream = ByteArrayInputStream(dummyContent.toByteArray(StandardCharsets.UTF_8))

        return DownloadedFile("${fileId}.txt", stream)
    }
}
