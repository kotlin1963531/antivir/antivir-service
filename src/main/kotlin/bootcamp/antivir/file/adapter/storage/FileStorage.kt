package bootcamp.antivir.file.adapter.storage

import bootcamp.antivir.file.dto.DownloadedFile
import java.util.UUID

interface FileStorage {
    suspend fun getFile(fileId: UUID): DownloadedFile
}
