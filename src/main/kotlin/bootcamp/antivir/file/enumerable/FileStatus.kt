package bootcamp.antivir.file.enumerable

enum class FileStatus {
    NOT_INFECTED, INFECTED
}
