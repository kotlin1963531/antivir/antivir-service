package bootcamp.antivir.file.adapter.storage

import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import java.util.*

@MicronautTest
class MockFileStorageTest() {

    private val mockFileStorage: MockFileStorage = MockFileStorage()

    @Test
    fun `should return correct file for valid UUID`() {
        val fileId = UUID.randomUUID()

        runBlocking {
            val downloadedFile = mockFileStorage.getFile(fileId)

            assertEquals("${fileId}.txt", downloadedFile.name)
        }
    }

    @Test
    fun `should throw exception for invalid UUID`() {
        val invalidUUID = "invalid-uuid"

        runBlocking {
            assertThrows<IllegalArgumentException> {
                mockFileStorage.getFile(UUID.fromString(invalidUUID))
            }
        }
    }
}
