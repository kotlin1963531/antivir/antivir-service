package bootcamp.antivir.file.dto

import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import jakarta.validation.Validator
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import java.io.ByteArrayInputStream


@MicronautTest
class DownloadedFileTest() {

    companion object {
        private const val VALID_NAME = "file.txt"
        private val INPUT_STREAM = ByteArrayInputStream("Dummy content".toByteArray())
    }

    @Inject
    lateinit var validator: Validator

    @Test
    fun `should create DownloadedFile with name and content`() {
        val downloadedFile = DownloadedFile(VALID_NAME, INPUT_STREAM)
        val violations = validator.validate(downloadedFile)

        assertAll(
            { assertEquals(VALID_NAME, downloadedFile.name) },
            { assertEquals(INPUT_STREAM, downloadedFile.content) },
            { assertTrue(violations.isEmpty()) }
        )
    }

    @Test
    fun `should not allow empty name`() {
        val downloadedFile = DownloadedFile("", INPUT_STREAM)

        val violations = validator.validate(downloadedFile)

        assertFalse(violations.isEmpty())
        assertTrue(violations.any { it.propertyPath.toString() == "name" })
    }
}
